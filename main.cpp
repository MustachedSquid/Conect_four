#include <iostream>
#include <stdlib.h>

#include "Game.h"

int main() {

    system("cls");


    Game game = Game();

    std::cout << "Connect four. But its bad..." << std::endl << "Made by a Squid."<< std::endl << std::endl;

    game.printArea();
    while(true) {


        int input;
        std::cout << "Input: ";
        std::cin >> input;

        if (input < 0 || input > 6) {
            break;
        }
        if(game.play(input)){
            break;
        }
        system("cls");

        game.printArea();

    }

    int wait;
    std::cin >> wait;

    return 0;
}
