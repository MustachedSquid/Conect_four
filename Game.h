//
// Created by ink on 04/10/2021.
//

#ifndef CONECCFOR_GAME_H
#define CONECCFOR_GAME_H

#include <iostream>


class Game {

private:
    int area[6][7];
    int currentPlayer; //1 or 2

    void changePlayer();
    void setPos(int x, int y);
    int findValidPos(int y);
    bool checkForWinner();
    bool checkCurrentPos(int x, int y);
    void start();

public:
    Game();
    ~Game();

    bool play(int y);
    void printArea();
    char areaToCharArray();

};


#endif //CONECCFOR_GAME_H
