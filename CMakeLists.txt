cmake_minimum_required(VERSION 3.20)
project(Coneccfor)

set(CMAKE_CXX_STANDARD 14)

add_executable(Coneccfor main.cpp Game.cpp Game.h)
