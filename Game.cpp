//
// Created by ink on 04/10/2021.
//

#include "Game.h"

#include <iostream>

Game::Game() {

    start();

}

Game::~Game() {

}

void Game::start() {

    currentPlayer = 1;

    for (int i = 0; i < (sizeof(area) / sizeof(area[i])); i++) {
        for (int j = 0; j < (sizeof(area[i]) / sizeof(area[i][j])); j++) {
            area[i][j] = 0;
        }
    }

}


void Game::setPos(int x, int y) {

    area[y][x] = currentPlayer;

}

void Game::changePlayer() {

    if (currentPlayer == 1) {
        currentPlayer = 2;
    } else {
        currentPlayer = 1;
    }

}

int Game::findValidPos(int x) {

    for (int y = 5; y >= 0; --y) {

        if (area[y][x] == 0) {
            return y;
        }

    }

    return -1;

}
bool Game::checkCurrentPos(int x, int y){

    if(area[y][x] == 0){
        return false;
    }else{
        int counter = 0;
        for (int i = 0; i < 4; ++i) {

            if(i+x >= 7){
                break;
            }
            if(area[y][x+i] == area[y][x]){
                counter++;
            }

        }

        if(counter==4){
            return true;
        }
        counter = 0;
        for (int i = 0; i < 4; ++i) {
            if(i+y >= 6){
                break;
            }
            if(area[y+i][x] == area[y][x]){
                counter++;
            }

        }

        if(counter==4){
            return true;
        }
        counter = 0;
        for (int i = 0; i < 4; ++i) {
            if(i+y >= 6 || i+x >= 7){
                break;
            }
            if(area[y+i][x+i] == area[y][x]){
                counter++;
            }

        }

        if(counter==4){
            return true;
        }

        counter = 0;
        for (int i = 0; i < 4; ++i) {
            if(i-y < 0 || i-x <0 ){
                break;
            }
            if(area[y-i][x-i] == area[y][x]){
                counter++;
            }

        }

        if(counter==4){
            return true;
        }

    }

    return false;
}

bool Game::checkForWinner() {

    for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 6; j++) {
            if(checkCurrentPos(i,j)){
                return true;
            }
        }
        std::cout << std::endl;
    }




    return false;

}


bool Game::play(int x) {


    if (x < 7 && x >= 0) {

        int y = findValidPos(x);

        if (y < 0) {
            return false;
        }

        setPos(x, y);


        if (checkForWinner()) {
            printArea();
            std::cout << "Player " << currentPlayer << " wins!" << std::endl;
            start();
            return true;
        }

        changePlayer();

    }

    return false;


}

//Prints ---------
void Game::printArea() {
    std::cout << "[0][1][2][3][4][5][6]" << std::endl << "---------------------" << std::endl;

    for (int i = 0; i < (sizeof(area) / sizeof(area[i])); i++) {
        for (int j = 0; j < (sizeof(area[i]) / sizeof(area[i][j])); j++) {
            if (area[i][j] == 0){
                std::cout << "[ ]";

            }else {

                std::cout << "[" << area[i][j] << "]";
            }
        }
        std::cout << std::endl;
    }
}

char Game::areaToCharArray() {
    return 'a';
}
